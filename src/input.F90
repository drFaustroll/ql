!! Author: Alin M Elena
!! Date: 17-05-2019
!! License: GPL-3.0 https://opensource.org/licenses/GPL-3.0
module inputs
  use constants, only : dp
  implicit none
  private
  public :: read_input
contains

  subroutine read_input(ifile,trajectory,format,species,cutoff,l,max_neighbours,bottom,top)
    character(len=*), intent(in)    :: ifile
    character(len=256), intent(out) :: trajectory
    character(len=10), intent(out)  :: format
    character(len=8), intent(out)  :: species
    integer, intent(out)            :: l,max_neighbours
    real(dp),intent(out)            :: cutoff,bottom(3),top(3)
    integer                         :: iu

    namelist /input/ trajectory,format,species,cutoff,l,bottom,top,max_neighbours
    open(file=trim(ifile),newunit=iu, action="read",status="old")
    read(iu,nml=input)
    close(iu)
  end subroutine
end module inputs

