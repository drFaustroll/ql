!! Author: Alin M Elena
!! Date: 31-03-2019
!! License: GPL-3.0 https://opensource.org/licenses/GPL-3.0
module neighbours

  use, intrinsic :: iso_fortran_env, only : eu => error_unit
  use constants, only : dp, pi,tolerance
  use numerics, only  : hs

  implicit none
  private
  type, public                :: neighbours_type
    real(kind=dp)             :: cutoff2=20.0_dp
    integer                   :: max_neighbours = 450
    integer,allocatable       :: list(:,:) ! (atoms,{nneigbours,neighnours}
    real(kind=dp),allocatable :: r2(:)
    integer, allocatable      :: specmap(:)
    integer                   :: n! number of atoms of a certain specie
    integer                   :: nbox
    integer, allocatable      :: box(:)
    real(kind = dp)           :: bottom(3),top(3)
  contains
    procedure, public         :: init
    procedure, public         :: filter_box
    procedure, public         :: build
    final                     :: cleanup
  end type neighbours_type

contains



  integer function filter_species(labels,species,aspec,n)
    character(len=*),intent(in)       :: species,labels(:)
    integer, intent(in)               :: n
    integer, allocatable, intent(out) :: aspec(:)

    integer              :: i
    integer, allocatable :: tmp(:)
    integer              :: k

    allocate(tmp(n))

    k = 0
    do i =1, n
      if (Trim(labels(i)) == Trim(species)) Then
        k = k +1
        tmp(k) = i
      end if
    end do

    allocate(aspec(k))
    aspec =  tmp(1:k)
    filter_species = k
  end function filter_species

  subroutine init(T,nAtoms,cutoff,species,labels,mn,bottom,top)
  class(neighbours_type)              :: T
    integer, intent(in)               :: nAtoms,mn
    real(dp),intent(in),optional      :: cutoff
    character(len=*),intent(in)       :: species,labels(:)
    real(kind=dp), intent(in)         :: bottom(3),top(3)


    if (present(cutoff)) T%cutoff2 = cutoff**2
    T%n = filter_species(labels,species,T%specmap,nAtoms)

    T%max_neighbours = mn
    T%bottom = bottom
    T%top = top
    ! since we further filter no need of allocation at this stafe
    !allocate(T%list(1:T%n,0:T%max_neighbours),T%r2(1:T%n))
    allocate(T%r2(1:T%n))
  end subroutine init

  subroutine sort(r,list,p)
    integer, intent(in)          :: p
    real(kind=dp), intent(inout) :: r(p)
    integer, intent(inout)       :: list(p)


    integer       :: i,j,ia
    real(kind=dp) :: a

    do i = 1,p-1
      do j = i+1,p
        if (r(i) > r(j)) then
          ia = list(i)
          list(i) = list(j)
          list(j) = ia
          a = r(i)
          r(i) = r(j)
          r(j) = a
        end if
      end do
    end do
  end subroutine sort

  subroutine filter_box(T,r)
  class(neighbours_type)      :: T
    real(kind=dp), intent(in) :: r(:,:)

    integer, allocatable :: tmp(:)
    integer :: i,ii,k

    k = 0
    allocate(tmp(T%n))
    do ii = 1, T%n
      i = T%specmap(ii)
      !this is sloppy
      if ( (T%bottom(1)<r(1,i).and.r(1,i)<T%top(1)).and.&
        (T%bottom(2)<r(2,i).and.r(2,i)<T%top(2)).and. &
        (T%bottom(3)<r(3,i).and.r(3,i)<T%top(3)) ) then
        k = k + 1
        tmp(k) = i
      end if
    end do
    T%nbox = k
    if (allocated(T%box)) deallocate(T%box)
    allocate(T%box(k))
    T%box = tmp(1:k)
    deallocate(tmp)
  end Subroutine filter_box

  subroutine build(T,r,h,hi)
  class(neighbours_type)      :: T
    real(kind=dp), intent(in) :: r(:,:)
    real(kind=dp), intent(in) :: h(3,3),hi(3,3)

    integer        :: i,j,k,ii,jj
    real(kind=dp ) :: rij(3),r2,si(3),sj(3),sij(3)


    call T%filter_box(r)
    if (allocated(T%list)) deallocate(T%list)
    allocate(T%list(1:T%nbox,0:T%max_neighbours))

    do ii = 1, T%nbox
      i = T%box(ii)
      k = 0
      si=hs(hi,r(:,i))
      do jj = 1, T%n
        j = T%specmap(jj)
        if (i /= j) then
          sj=hs(hi,r(:,j))
          sij=si-sj
          sij=sij-nint(sij)
          rij=hs(h,sij)
          r2 = dot_product(rij,rij)
          if (r2 < T%cutoff2) then
            k = k + 1
            if (k > T%max_neighbours) then
              write(eu,*)"more neighbours than max_neigbours, increase and rerun"
              stop -101
            end if
            T%list(ii,k) = j
            T%r2(k) = r2
          end if
        end if
      end do
      T%list(ii,0) = k
      !call sort(T%r2,T%list(i,1:k),k)
      !     write(0,*)i,": ",k, " -- ", T%list(i,1:k)
      !     write(0,*)i,": ",k, " -- ", T%r2(1:k)
    end do
  end subroutine build

  subroutine cleanup(T)
    type(neighbours_type) :: T
    if (allocated(T%list)) deallocate(T%list)
    if (allocated(T%r2)) deallocate(T%r2)
  end subroutine cleanup
end module neighbours
